import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { debounceTime, delay, tap, filter, map, takeUntil } from 'rxjs/operators';

import { Employee, EMPLOYEES } from '../../data.service';

@Component({
  selector: 'filter-karyawan',
  templateUrl: './filter-karyawan.component.html',
  styleUrls: ['./filter-karyawan.component.css']
})
export class FilterKaryawanComponent implements OnInit, OnDestroy {

  protected employees: Employee[] = EMPLOYEES;

  public listEmployeeCtrl: FormControl = new FormControl();

  public listEmployeeFilteringCtrl: FormControl = new FormControl();

  public searching: boolean = false;

  public filteredEmployee: ReplaySubject<Employee[]> = new ReplaySubject<Employee[]>(1);

  protected _onDestroy = new Subject<void>();

  ngOnInit() {

    // listen for search field value changes
    this.listEmployeeFilteringCtrl.valueChanges
      .pipe(
        filter(search => !!search),
        tap(() => this.searching = true),
        takeUntil(this._onDestroy),
        debounceTime(200),
        map(search => {
          if (!this.employees) {
            return [];
          }

          // simulate server fetching and filtering data
          return this.employees.filter(employee => employee.employeeName.toLowerCase().indexOf(search) > -1);
        }),
        delay(500)
      )
      .subscribe(filteredEmployees => {
        this.searching = false;
        this.filteredEmployee.next(filteredEmployees);
      },
        error => {
          // no errors in our simulated example
          this.searching = false;
          // handle error...
        });

  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

}
