import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from  '@angular/material';
import { FormControl } from '@angular/forms';
import { DataService } from '../data.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  listEmpBpjs = [];
  panelOpenState = true;
  public listEmployeeCtrl: FormControl = new FormControl();
  public listEmployeeFilteringCtrl: FormControl = new FormControl();
  public searching: boolean = false;

  @ViewChild('filterEmployee') filterEmployee: MatAccordion;

  constructor(private data: DataService) { }

  ngOnInit() {
    this.listEmpBpjs = this.data.getListEmpBpjs();
    this.panelOpenState = true;
  }

  filter() {
    this.panelOpenState = false;
  }
}
