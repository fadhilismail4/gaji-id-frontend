import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Employee {
  id: number;
  nik: number;
  employeeName: string;
}

export const EMPLOYEES: Employee[] = [
  {
    id: 1,
    nik: 123,
    employeeName: 'Jono'
  },
  {
    id: 2,
    nik: 234,
    employeeName: 'Susan'
  }
];

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getListEmpBpjs() {
    // return this.http.get('https://reqres.in/api/users')
    return [
      {
        employeeId: 1,
        employeeName: 'Jono',
        bpjsTkFlag: false,
        nppBpjsTkId: 123,
        nppBpjsTkName: 'string',
        jpFlag: true,
        noKartuBpjsTk: 123,
        flagGajiAcuanBpjsTk: true,
        gajiAcuanBpjsTk: 123,
        bpjsKesFlag: true,
        nppBpjsKesId: 1123,
        nppBpjsKesName: 'pegawai',
        noKartuBpjsKes: 123123,
        flagGajiAcuanBpjsKes: true,
        gajiAcuanBpjsKes: 'uuuaa'
      },
      {
        employeeId: 2,
        employeeName: 'Susan',
        bpjsTkFlag: true,
        nppBpjsTkId: 123,
        nppBpjsTkName: 'string',
        jpFlag: true,
        noKartuBpjsTk: 123,
        flagGajiAcuanBpjsTk: true,
        gajiAcuanBpjsTk: 123,
        bpjsKesFlag: true,
        nppBpjsKesId: 1123,
        nppBpjsKesName: 'pegawai',
        noKartuBpjsKes: 123123,
        flagGajiAcuanBpjsKes: true,
        gajiAcuanBpjsKes: 'uuuaa'
      }
    ];
  }

  getListAuthEmployee() {
    return [
      {
        id: 1,
        nik: 123,
        employeeName: 'Jono'
      },
      {
        id: 2,
        nik: 234,
        employeeName: 'Susan'
      }
    ]
  }
}
